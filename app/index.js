$( document ).ready(function() {
  $(".custom-input__input").focus(function(){
  $(this).parent().addClass("active completed");
  });

  $(".custom-input__input").focusout(function(){
  if($(this).val() === "")
    $(this).parent().removeClass("completed");
  $(this).parent().removeClass("active");
  })

  $("#edit").on('click',function() {
    $('#fields').addClass('block');
    $('.about').addClass('hide');
  })

  $("cancel").on('click', function() {
    $('#fields').removeClass('block');
    $('.about').removeClass('hide');
  })

  $('#edit-web').on('click',function(){
    $('#modal-web').toggle('modal--block');
  });

  $('#edit-phone').on('click',function(){
    $('#modal-phone').toggle('modal--block');
  });

  $('#edit-address').on('click',function(){
    $('#modal-address').toggle('modal--block');
  });

  $('#modal-btn-save-web , #modal-btn-cancel-web').on('click',function() {
      $('#modal-web').toggle('modal--block');
  })

  $('#modal-btn-save-phone , #modal-btn-cancel-phone').on('click',function() {
      $('#modal-phone').toggle('modal--block');
  })

  $('#modal-btn-save-address , #modal-btn-cancel-address').on('click',function() {
      $('#modal-address').toggle('modal--block');
  })
} );
